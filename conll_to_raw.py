import tagger


def main(args):
    corpus = tagger.read_conll(args.corpus)
    with open(args.output, "w") as f:
        for sentence in corpus:
            tokens = [tok[tagger.FORM] for tok in sentence]
            f.write("{}\n".format(" ".join(tokens)))

if __name__ == "__main__":
    import argparse
    usage = """Read a conllu corpus and prints
    one sentence per line in a new file """

    parser = argparse.ArgumentParser(description = usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("corpus", help="Input corpus")
    parser.add_argument("output", help="Output file")

    args = parser.parse_args()

    main(args)


