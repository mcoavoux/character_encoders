
from collections import defaultdict
import sys
import os
import logging
import random


import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

from character_encoders import CharacterLstmLayer, CharacterConvolutionLayer

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


ID,FORM,LEMMA,UPOS,XPOS,FEATS,HEAD,DEPREL,DEPS,MISC=range(10)


def read_conll(filename):
    """
        returns list of sentences
        sentence = list of tokens
        token = list of fields

        ex: sentences[0][0][FORM] is the word form of the first token of the first sentence
    """
    with open(filename) as f :
        sentences = [[ line.split("\t") 
                       for line in sen.split("\n") 
                       if line and line[0] != "#"]
                       for sen in f.read().split("\n\n") if sen.strip()]

    for i in range(len(sentences)):
        # la ligne suivante retire les amalgames
        sentences[i] = [t for t in sentences[i] if "-" not in t[ID]]
        s = sentences[i]
        for tok in s:
            tok[ID] = int(tok[ID])
            tok[HEAD] = int(tok[HEAD])
    return sentences

def get_data_from_conllu(conllu):
    res = []
    for sentence in conllu:
        tokens = [t[FORM] for t in sentence]
        tags = [t[UPOS] for t in sentence]
        res.append((tokens, tags))
    return res

def get_voc(corpus):
    tagset = defaultdict(int)
    voc = defaultdict(int)
    for toks, tags in corpus:

        for tok in toks:
            for c in tok:
                voc[c] += 1

        for tag in tags:
            tagset[tag] += 1
    return tagset, voc





class Tagger(nn.Module):
    def __init__(self, vocab_size, output_size, embedding_dim, lstm_input_dim, lstm_output_dim):
        super(Tagger, self).__init__()
    
        self.char_encoder = CharacterConvolutionLayer(embedding_dim, vocab_size, lstm_input_dim)
        self.word_transducer = nn.LSTM(lstm_input_dim, 
                                       lstm_output_dim//2,
                                       num_layers=1,
                                       bidirectional=True,
                                       dropout=0,
                                       batch_first=True)

        self.linear = nn.Linear(lstm_output_dim, output_size)
        self.logsoftmax = nn.LogSoftmax(dim=1)


    def forward(self, input):
    
        embeddings = self.char_encoder(input)
        contextual_embeddings, _ = self.word_transducer(embeddings.unsqueeze(0))

        last = self.linear(contextual_embeddings.squeeze(0))
        out = self.logsoftmax(last)
        return out

def corpus_to_tensors(corpus, char2i, tag2i, device):
    tensors = []
    start, stop = [char2i["<START>"]], [char2i["<STOP>"]]
    for toks, tags in corpus:
        chars_idx =  [start 
                      + [char2i[c] if c in char2i else char2i["<UNK>"] for c in tok]
                      + stop
                      for tok in toks]
        tags_idx = [tag2i[tag] for tag in tags]

        tensors.append(
            ([torch.tensor(chars, dtype=torch.long).to(device) for chars in chars_idx],
             torch.tensor(tags_idx, dtype=torch.long).to(device))
        )
    return tensors

def sentence_to_tensors(sentence, char2i, device):
    tensors = []
    start, stop = [char2i["<START>"]], [char2i["<STOP>"]]
    chars = [start 
            + [char2i[c] if c in char2i else char2i["<UNK>"] for c in tok] 
            + stop
             for tok in sentence ]
    return [torch.tensor(cs, dtype=torch.long).to(device) for cs in chars]

def stochastic_replacement(device, long_tensor, p=0.2):
    """Replaces elements of long_tensor by 1 (code for <UNK>)
    with probability p."""
    mask = torch.rand(len(long_tensor), device=device) > (1-p)
    mask[0] = 0
    mask[-1] = 0
    copy_tensor = torch.tensor(long_tensor, device=device)
    copy_tensor[mask] = 1
    return copy_tensor


def evaluate(tagger, tensors, loss_fn):
    loss = 0
    acc = 0
    examples = 0

    for toks, tags in tensors:

        output = tagger(toks)
        predictions = torch.argmax(output, dim=1).cpu()
        
        pointwiseloss = loss_fn(output, tags)
        sum_loss = torch.sum(pointwiseloss)

        loss += sum_loss.float()
        examples += len(toks)
        acc += np.sum(predictions.numpy() == tags.cpu().numpy())

    return loss / examples, 100 * acc / examples


def train_epoch(tagger, optimizer, loss_fn, char2i, tag2i, train_tensors, dev_tensors, sample_train_tensors, device):
    epoch_loss = 0
    n_examples = 0

    tagger.train()
    for i, (tok_tensors, tag_tensors) in enumerate(train_tensors):
        optimizer.zero_grad()

        tok_tensors = [stochastic_replacement(device, tok) for tok in tok_tensors]

        output = tagger(tok_tensors)

        loss = loss_fn(output, tag_tensors)
        sum_loss = torch.sum(loss)

        epoch_loss += sum_loss.float()

        sum_loss.backward()
        optimizer.step()

        n_examples += len(tok_tensors)

        if i % 250 == 0:
            logging.info("train {} / {}".format(i, len(train_tensors)))

    with torch.no_grad():
        dev_loss, dev_acc = evaluate(tagger, dev_tensors, loss_fn)
        train_loss, train_acc = evaluate(tagger, sample_train_tensors, loss_fn)

    return {"learning loss": epoch_loss / n_examples,
            "dev loss": dev_loss,
            "dev acc": dev_acc,
            "train loss": train_loss,
            "train acc": train_acc}




def main_eval(args, device):
    i2char, char2i = load_voc("{}/chars".format(args.model))
    i2tag, tag2i = load_voc("{}/tags".format(args.model))

    tagger = torch.load("{}/model".format(args.model))
    tagger.to(device)

    with open(args.test) as f:
        sentences = [line.strip().split() for line in f]
    
    all_predictions = []

    sentence_tensors = [sentence_to_tensors(sent, char2i, device) for sent in sentences]
    for sent_tensors in sentence_tensors:

        output = tagger(sent_tensors)
        predictions_idx = torch.argmax(output, dim=1).cpu()
        predictions_str = [i2tag[i] for i in predictions_idx]

        all_predictions.append(predictions_str)

    line = "\t".join(["{}" for i in range(10)])
    for sent, tags in zip(sentences, all_predictions):
        for i, tok, tag in zip(range(len(sent)), sent, tags):
            print(line.format(i+1, tok, "_", tag, tag, "_", i, "_", "_", "_"))
        print()



def save_voc(l, filename):
    with open(filename, "w") as f:
        for item in l:
            f.write("{}\n".format(item))

def load_voc(filename):
    with open(filename, "r") as f:
        voc = [line.strip() for line in f]
        voc2i = {tok: i for i, tok in enumerate(voc)}
    return voc, voc2i


def main_train(args, device):

    torch.manual_seed(args.s)
    np.random.seed(args.s)
    random.seed(args.s)

    train = get_data_from_conllu(read_conll(args.train))
    dev = get_data_from_conllu(read_conll(args.dev))
    
    tagset, voc = get_voc(train)

    logging.info("Tagset size {}".format(len(tagset)))
    
    i2tag = sorted(tagset)
    tag2i = {k:i for i, k in enumerate(i2tag)}
    
    i2char = ["<PAD>", "<UNK>", "<START>", "<STOP>"] + sorted(voc)
    char2i = {k:i for i, k in enumerate(i2char)}


    os.makedirs(args.model, exist_ok = True)
    save_voc(i2tag, "{}/tags".format(args.model))
    save_voc(i2char, "{}/chars".format(args.model))


    tagger = Tagger(len(i2char), len(i2tag), args.c, args.w, args.W)
    tagger.to(device)

    optimizer = optim.SGD(tagger.parameters(), lr = 0.01, momentum=0.9)

    loss_fn = nn.NLLLoss()

    train_tensors = corpus_to_tensors(train, char2i, tag2i, device)
    dev_tensors = corpus_to_tensors(dev, char2i, tag2i, device)

    random.shuffle(train_tensors)
    sample_train_tensors = train_tensors[:len(dev_tensors)]
    
    best_dev_acc = 0

    summary = "Epoch {} train: l={:.4f} acc={:.2f} dev: l={:.4f} acc={:.2f}"
    for e in range(args.e):
        ev = train_epoch(tagger, optimizer, loss_fn, char2i, tag2i, train_tensors, dev_tensors, sample_train_tensors, device)
        print(summary.format(e, 
                             ev["train loss"], ev["train acc"], 
                             ev["dev loss"],   ev["dev acc"]), 
              flush=True)
        if ev["dev acc"] > best_dev_acc:
            best_dev_acc = ev["dev acc"]
            tagger.cpu()
            torch.save(tagger, "{}/model".format(args.model))
            tagger.to(device)

def main(args, device):
    if args.mode == "train":
        main_train(args, device)
    elif args.mode == "eval":
        main_eval(args, device)
    else:
        assert False, "Should not reach this point"

if __name__ == "__main__":
    import argparse
    usage = ""
    parser = argparse.ArgumentParser(description = usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    subparsers = parser.add_subparsers(dest="mode", description="Execution modes", help='train: training, eval: test')

    train_parser = subparsers.add_parser("train", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    eval_parser = subparsers.add_parser("eval", formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    train_parser.add_argument("model", help="Model output")
    train_parser.add_argument("train", help="Training corpus")
    train_parser.add_argument("dev", help="Validation corpus")

    train_parser.add_argument("--gpu", type=int, default=None, help="use gpu if available")
    train_parser.add_argument("-c", type=int, default=50, help="Character embedding size")
    train_parser.add_argument("-w", type=int, default=100, help="Word embedding size (character-based)")
    train_parser.add_argument("-W", type=int, default=100, help="Word lstm output size")
    train_parser.add_argument("-e", type=int, default=30, help="Training epochs")
    train_parser.add_argument("-l", type=float, default=0.01, help="Learning rate")
    train_parser.add_argument("-s", type=int, default=42, help="Random seed")


    eval_parser.add_argument("model", help="Model output")
    eval_parser.add_argument("--test", default=None, help="Test corpus, read stdin if not provided")
    eval_parser.add_argument("--gpu", type=int, default=None, help="use gpu if available")


    args = parser.parse_args()

    use_cuda = torch.cuda.is_available()
    if use_cuda and args.gpu is not None:
        device = torch.device("cuda:{}".format(args.gpu))
    else:
        device = torch.device("cpu")

    main(args, device)







